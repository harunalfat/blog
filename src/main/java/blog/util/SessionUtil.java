package blog.util;

import blog.data.enumeration.EnumSessionVariables;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SessionUtil {

	public static Boolean isLogged(HttpSession session, HttpServletResponse response) throws IOException, HttpClientErrorException{
		if(session.getAttribute(EnumSessionVariables.user.toString()) == null){
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "not logged");
		}
		return true;
	}
	
}
